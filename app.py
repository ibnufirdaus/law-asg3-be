import os

from flask import Flask, flash, request, redirect, render_template, jsonify, make_response
import pika
import sys
import datetime
import urllib.request
from werkzeug.utils import secure_filename
from flask_cors import CORS, cross_origin
from io import BytesIO
import zipfile
import multiprocessing as mp
import time

ALLOWED_EXTENSIONS = {'txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif'}

UPLOAD_FOLDER = 'uploads/'

app = Flask(__name__)
app.secret_key = "secret key"
cors = CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['MAX_CONTENT_LENGTH'] = 16 * 1024 * 1024


@app.route('/')
def index():
    return "Site is working properly."


#== Fungsi untuk melakukan pengiriman pesan ke message broker
#== Routing key sudah custom
def send_message(routing_key, message):
    credentials = pika.PlainCredentials(username="0806444524", password="0806444524")
    parameters = pika.ConnectionParameters(host="152.118.148.95", port=5672, virtual_host="/0806444524",
                                           credentials=credentials)
    connection = pika.BlockingConnection(parameters=parameters)
    channel = connection.channel()

    channel.exchange_declare(exchange='1606893986', exchange_type='direct')

    channel.basic_publish(exchange='1606893986', routing_key=routing_key, body=message)
    connection.close()


def allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


#== FUngsi untuk melakukan kompresi berkas
#== Fungsi ini dijalnkan oleh multprocessing.Pool sehingga berjalan async terhadap fungsi utama
def zip_file(filepath):
    send_message(request.headers.get("X-ROUTING-KEY"), "Starting compression")
    time.sleep(1.25)
    filenames = [filepath]

    zip_subdir = "result"

    s = BytesIO()

    zf = zipfile.ZipFile(s, "w")

    time.sleep(1.25)
    send_message(request.headers.get("X-ROUTING-KEY"), "Compressing")

    # Proses ini dilakukan agar ada kesan progress ke user
    # Python tidak menyediakan kompresi dengan progres
    # sehingga saya menggunakna cara ini, maksa memang,
    # tapi menurut saya masih sesuai dengan tujuan pembelajaran, yaitu async message queue
    file = open(filepath, 'rb')
    byte_length = file.seek(0, 2)
    file.seek(0, 0)
    i = 0.1
    speed = file.tell() // 200 + 1
    while file.tell() < byte_length:
        file.read(speed)

        # mengirimkan pesan tiap ~10%
        if file.tell() / byte_length >= i:
            send_message(request.headers.get("X-ROUTING-KEY"), str(file.tell() * 100 / byte_length) + "% Compressed")
            i += 0.1
    send_message(request.headers.get("X-ROUTING-KEY"), str(file.tell() * 100 / byte_length) + "% Compressed")
    file.close()


    # Berkas dikompresi disini
    for fpath in filenames:
        fdir, fname = os.path.split(fpath)
        zip_path = os.path.join(zip_subdir, fname)

        zf.write(fpath, zip_path)

    zf.close()
    s.flush()
    time.sleep(1.25)
    send_message(request.headers.get("X-ROUTING-KEY"), "Compression completed")

    os.remove(filepath)


#== Fungsi utama untuk menerima file dari server 1
@app.route('/upload', methods=['POST'])
@cross_origin()
def upload_file():
    response = {}

    # Prosedur error checking standar, copas dari internet mostly
    if request.method == 'POST':
        if 'file' not in request.files:
            response["message"] = 'No file part'
            return make_response(jsonify(response))
        file = request.files['file']
        if file.filename == '':
            response["message"] = 'No file selected for uploading'
            return make_response(jsonify(response))

        # File valid
        if file and allowed_file(file.filename):
            send_message(request.headers.get("X-ROUTING-KEY"), "File valid, waiting for upload completion")
            # Menyimpan file
            filename = secure_filename(file.filename)
            filepath = os.path.join(app.config['UPLOAD_FOLDER'], filename)
            file.save(filepath)
            response["message"] = 'File successfully uploaded -> ' + filepath

            # memanggil Pool untuk melakukan task kompresi secara async
            pool = mp.Pool()
            pool.apply_async(zip_file, args=(filepath,))
            return make_response(jsonify(response))
        else:
            response["message"] = 'Allowed file types are txt, pdf, png, jpg, jpeg, gif'
            return make_response(jsonify(response))
    else:
        response["message"] = 'Not a POST request'
        return make_response(jsonify(response))


if __name__ == '__main__':
    app.run(host="0.0.0.0", port=20317)
    # app.run(host="127.0.0.1", port=5002)
